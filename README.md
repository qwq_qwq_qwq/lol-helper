# lol-Helper
## 软件介绍
  使用英雄联盟官方提供的客户端Api,实现各种功能。
## 使用技术
  java spring boot    
## 使用方法
  * 先打开英雄联盟客户端并且登陆游戏
  * 运行项目,访问http://localhost:8080/index.html
## 主要功能
### 战绩查询
选英雄时查询队友战绩并且按照近6局游戏战绩计算平均kda，实现等级排名（上等马，下等马，牛马等）同时可以发送到聊天框
,游戏开始时可以查询敌方战绩
### 段位修改
可以修改自己的段位（单双，灵活），好友可见
## 效果图预览
#### 显示当前召唤师信息
![image](https://gitee.com/qwq_qwq_qwq/pic/raw/master/1.jpg)
#### 段位修改为最强王者
![image](https://gitee.com/qwq_qwq_qwq/pic/raw/master/2.jpg)
#### 游戏选人时分析战绩
![image](https://gitee.com/qwq_qwq_qwq/pic/raw/master/3.jpg)
#### 战绩列表查看
![image](https://gitee.com/qwq_qwq_qwq/pic/raw/master/4.jpg)
