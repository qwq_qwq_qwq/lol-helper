package com.qwq.model;

import lombok.Data;

@Data
public class LcuApiInfo {
    // 端口
    private String port;
    // token
    private String token;
}
