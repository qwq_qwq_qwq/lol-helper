package com.qwq.service;

import com.qwq.dto.GameRecordInputDto;
import com.qwq.dto.GameRecordOutputDto;
import com.qwq.dto.SummonerInfoOutputDto;

import java.util.List;

/**
 * 程序主要功能，利用lcuApi实现一些自定义功能,给网页使用
 */
public interface AppService {
    SummonerInfoOutputDto getCurrentSummonerInfo();

    String getArea(String areaCode);

    List<GameRecordOutputDto> GameRecordAnalyse(GameRecordInputDto inputDto);
}
