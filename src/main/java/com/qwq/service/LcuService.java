package com.qwq.service;

import com.qwq.dto.SendRoomMessageInputDto;
import com.qwq.dto.lcu.*;

import java.util.List;

/*
 * 封装lcuApi接口
 * */
public interface LcuService {
    LolChampSelectV1SessionResponse getRoomInfo();

    List<LolChatConversationsRoomNameMessageResponse> getMessagesByRoomName(String roomName);

    LolSummonerInfoResponse getSummonerInfo(String summonerId);

    LolMatchHistoryResponse getMatchHistoryInfo(String puuid, int beginIndex, int endIndex);

    LolSummonerInfoResponse getLoginSummonerInfo();

    ReportingEnvironmentResponse getRiotclientV1CrashReportingEnvironment();

    byte[] getAvatarImg(String id);

    LolBlueEssenceResponse getCurrentLolBlueEssenceResponse();

    ChampionInfoResponse getChampionInfo(int id);

    boolean sendRoomMessage(SendRoomMessageInputDto inputDto);

    LolRankedV1RankedStatsResponse getRankedInfo(String puuid);

    void updateCurrentSummonerRankedInfo(LolUpdateRankedRequest request);

    LolSummonerInfoResponse getSummonerInfoByName(String name);
}
