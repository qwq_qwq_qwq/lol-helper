package com.qwq.service.impl;

import com.qwq.dto.gameData.AllGameDataResponse;
import com.qwq.service.GameDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GameDataServiceImpl implements GameDataService {
    private final RestTemplate _restTemplate;

    @Autowired
    public GameDataServiceImpl(RestTemplate restTemplate) {
        _restTemplate = restTemplate;
    }

    @Override
    public AllGameDataResponse getAllGameData() {
        String requestUrl = "https://localhost:2999/liveclientdata/allgamedata";
        return _restTemplate.getForObject(requestUrl, AllGameDataResponse.class);
    }
}
