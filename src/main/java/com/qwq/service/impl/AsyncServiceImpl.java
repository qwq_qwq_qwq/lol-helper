package com.qwq.service.impl;

import com.qwq.dto.GameRecordOutputDto;
import com.qwq.dto.SendRoomMessageInputDto;
import com.qwq.infrastructure.exception.StringResponseException;
import com.qwq.service.AsyncService;
import com.qwq.service.LcuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AsyncServiceImpl implements AsyncService {
    private final LcuService _lcuService;

    @Autowired
    public AsyncServiceImpl(LcuService lcuService) {
        _lcuService = lcuService;
    }

    @Async
    public void sendToTeammate(List<GameRecordOutputDto> outputDtoList, String roomName) {
        for (GameRecordOutputDto temp : outputDtoList) {
            String rankSolo = "单双:" + temp.getRankedSolo().getRank();
            if (!temp.getRankedSolo().getRank().equals("未完成定位赛")) {
                rankSolo += "当前胜点:" + temp.getRankedSolo().getPoint() + "胜场:" + temp.getRankedSolo().getWin();
            }
            String rankLingHuo = "灵活:" + temp.getRankedLingHuo().getRank();
            if (!temp.getRankedLingHuo().getRank().equals("未完成定位赛")) {
                rankLingHuo += "当前胜点:" + temp.getRankedLingHuo().getPoint() + "胜场:" + temp.getRankedLingHuo().getWin();
            }

            StringBuilder stringBuilder = new StringBuilder("我方[" + temp.getAssessment() + "] " + temp.getSummonerName() + " " + rankSolo + " " + rankLingHuo + " ,近期战绩: ");
            for (var gameDetailsTemp : temp.getGameDetails()) {
                stringBuilder.append(gameDetailsTemp.getChampionName())
                        .append("(")
                        .append(gameDetailsTemp.getKill())
                        .append("-")
                        .append(gameDetailsTemp.getDeath())
                        .append("-")
                        .append(gameDetailsTemp.getAssist())
                        .append(")").append(" ");
            }
            stringBuilder.append(" 平均kda:").append(temp.getAvgKda());
            _lcuService.sendRoomMessage(new SendRoomMessageInputDto(roomName, stringBuilder.toString(), String.valueOf(temp.getSummonerId())));
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new StringResponseException(e.getMessage());
            }
        }
    }
}
