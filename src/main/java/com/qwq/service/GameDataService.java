package com.qwq.service;

import com.qwq.dto.gameData.AllGameDataResponse;

public interface GameDataService {
    AllGameDataResponse getAllGameData();
}
