package com.qwq.service;

import com.qwq.dto.GameRecordOutputDto;

import java.util.List;

public interface AsyncService {
    void sendToTeammate(List<GameRecordOutputDto> outputDtoList, String roomName);
}
