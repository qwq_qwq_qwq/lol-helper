package com.qwq.controller;

import com.qwq.dto.SendRoomMessageInputDto;
import com.qwq.dto.lcu.*;
import com.qwq.service.LcuService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@ApiOperation("lcu基础api")
@RestController
@RequestMapping("/lcu")
public class LcuController {
    private final LcuService _lcuService;

    @Autowired
    public LcuController(LcuService lcuService) {
        this._lcuService = lcuService;
    }

    @ApiOperation("获取选英雄时房间信息")
    @GetMapping("/room")
    public LolChampSelectV1SessionResponse getRoomInfo() {
        return _lcuService.getRoomInfo();
    }

    @ApiOperation("获取选英雄时房间聊天信息")
    @GetMapping("/room/messages")
    public List<LolChatConversationsRoomNameMessageResponse> getMessagesByRoomName(@ApiParam("房间id") String roomName) {
        return _lcuService.getMessagesByRoomName(roomName);
    }


    @ApiOperation("选英雄时发送消息")
    @PostMapping(value = "/room/messages")
    public boolean sendRoomMessage(@RequestBody SendRoomMessageInputDto inputDto) {
        return _lcuService.sendRoomMessage(inputDto);
    }

    @ApiOperation("获取召唤师信息")
    @GetMapping("/summoner")
    public LolSummonerInfoResponse getSummonerInfo(@ApiParam("召唤师id") String summonerId) {
        return _lcuService.getSummonerInfo(summonerId);
    }

    @ApiOperation("查询战绩")
    @GetMapping("/match/history")
    public LolMatchHistoryResponse getMatchHistoryInfo(@ApiParam("玩家唯一id") String puuid, @ApiParam("开始索引") int beginIndex, @ApiParam("结束索引") int endIndex) {
        return _lcuService.getMatchHistoryInfo(puuid, beginIndex, endIndex);
    }

    @ApiOperation("获取当前登陆召唤师信息")
    @GetMapping("/summoner/current")
    public LolSummonerInfoResponse getLoginSummonerInfo() {
        return _lcuService.getLoginSummonerInfo();
    }

    @ApiOperation("修改当前召唤师段位")
    @PutMapping("/summoner/current/ranked")
    public void updateCurrentSummonerRankedInfo(@RequestBody LolUpdateRankedRequest request) {
        _lcuService.updateCurrentSummonerRankedInfo(request);
    }

    @ApiOperation("获取当前客户端环境信息")
    @GetMapping("/riot/client/environment")
    public ReportingEnvironmentResponse getRiotclientV1CrashReportingEnvironment() {
        return _lcuService.getRiotclientV1CrashReportingEnvironment();
    }

    @ApiOperation("获取头像图片")
    @GetMapping(value = "/avatar", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getAvatarImg(String id) {
        return _lcuService.getAvatarImg(id);
    }

    @ApiOperation("获取当前登陆用户蓝色精粹")
    @GetMapping(value = "/essence/blue")
    public LolBlueEssenceResponse getCurrentLolBlueEssence() {
        return _lcuService.getCurrentLolBlueEssenceResponse();
    }

    @ApiOperation("获取段位信息")
    @GetMapping(value = "/ranked/info")
    public LolRankedV1RankedStatsResponse getRankedInfo(String puuid) {
        return _lcuService.getRankedInfo(puuid);
    }

    @ApiOperation("根据名称获取召唤师信息")
    @GetMapping("/summoner/name")
    public LolSummonerInfoResponse getSummonerInfoByName(String name) {
        return _lcuService.getSummonerInfoByName(name);
    }
}
