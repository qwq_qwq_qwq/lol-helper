package com.qwq.controller;

import com.qwq.dto.GameRecordInputDto;
import com.qwq.dto.GameRecordOutputDto;
import com.qwq.dto.SummonerInfoOutputDto;
import com.qwq.service.AppService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@ApiOperation("网页AppApi，整合了LcuApi实现自定义功能")
@RestController
@RequestMapping("/app")
public class AppController {
    private final AppService _appService;

    @Autowired
    public AppController(AppService appService) {
        _appService = appService;
    }

    @ApiOperation("获取当前召唤师信息，包括蓝色精粹等")
    @GetMapping("/current/summoner/info")
    public SummonerInfoOutputDto getCurrentSummonerInfo() {
        return _appService.getCurrentSummonerInfo();
    }

    @ApiOperation("选英雄时分析房间内召唤师游戏战绩")
    @PostMapping("/game/record/analyse")
    public List<GameRecordOutputDto> GameRecordAnalyse(@RequestBody GameRecordInputDto inputDto) {
        return _appService.GameRecordAnalyse(inputDto);
    }
}
