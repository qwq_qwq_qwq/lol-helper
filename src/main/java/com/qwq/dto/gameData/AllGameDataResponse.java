package com.qwq.dto.gameData;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class AllGameDataResponse {

    @JsonProperty("activePlayer")
    private ActivePlayerDto activePlayer;
    @JsonProperty("allPlayers")
    private List<AllPlayersDto> allPlayers;


    @NoArgsConstructor
    @Data
    public static class ActivePlayerDto {
        @JsonProperty("abilities")
        private AbilitiesDto abilities;
        @JsonProperty("championStats")
        private ChampionStatsDto championStats;
        @JsonProperty("currentGold")
        private Double currentGold;
        @JsonProperty("fullRunes")
        private FullRunesDto fullRunes;
        @JsonProperty("level")
        private Integer level;
        @JsonProperty("summonerName")
        private String summonerName;
        @JsonProperty("teamRelativeColors")
        private Boolean teamRelativeColors;

        @NoArgsConstructor
        @Data
        public static class AbilitiesDto {
            @JsonProperty("E")
            private EDto e;
            @JsonProperty("Passive")
            private PassiveDto passive;
            @JsonProperty("Q")
            private QDto q;
            @JsonProperty("R")
            private RDto r;
            @JsonProperty("W")
            private WDto w;

            @NoArgsConstructor
            @Data
            public static class EDto {
                @JsonProperty("abilityLevel")
                private Integer abilityLevel;
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private String id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class PassiveDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private String id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class QDto {
                @JsonProperty("abilityLevel")
                private Integer abilityLevel;
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private String id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class RDto {
                @JsonProperty("abilityLevel")
                private Integer abilityLevel;
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private String id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class WDto {
                @JsonProperty("abilityLevel")
                private Integer abilityLevel;
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private String id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }
        }

        @NoArgsConstructor
        @Data
        public static class ChampionStatsDto {
            @JsonProperty("abilityHaste")
            private Double abilityHaste;
            @JsonProperty("abilityPower")
            private Double abilityPower;
            @JsonProperty("armor")
            private Double armor;
            @JsonProperty("armorPenetrationFlat")
            private Double armorPenetrationFlat;
            @JsonProperty("armorPenetrationPercent")
            private Double armorPenetrationPercent;
            @JsonProperty("attackDamage")
            private Double attackDamage;
            @JsonProperty("attackRange")
            private Double attackRange;
            @JsonProperty("attackSpeed")
            private Double attackSpeed;
            @JsonProperty("bonusArmorPenetrationPercent")
            private Double bonusArmorPenetrationPercent;
            @JsonProperty("bonusMagicPenetrationPercent")
            private Double bonusMagicPenetrationPercent;
            @JsonProperty("critChance")
            private Double critChance;
            @JsonProperty("critDamage")
            private Double critDamage;
            @JsonProperty("currentHealth")
            private Double currentHealth;
            @JsonProperty("healShieldPower")
            private Double healShieldPower;
            @JsonProperty("healthRegenRate")
            private Double healthRegenRate;
            @JsonProperty("lifeSteal")
            private Double lifeSteal;
            @JsonProperty("magicLethality")
            private Double magicLethality;
            @JsonProperty("magicPenetrationFlat")
            private Double magicPenetrationFlat;
            @JsonProperty("magicPenetrationPercent")
            private Double magicPenetrationPercent;
            @JsonProperty("magicResist")
            private Double magicResist;
            @JsonProperty("maxHealth")
            private Double maxHealth;
            @JsonProperty("moveSpeed")
            private Double moveSpeed;
            @JsonProperty("omnivamp")
            private Double omnivamp;
            @JsonProperty("physicalLethality")
            private Double physicalLethality;
            @JsonProperty("physicalVamp")
            private Double physicalVamp;
            @JsonProperty("resourceMax")
            private Double resourceMax;
            @JsonProperty("resourceRegenRate")
            private Double resourceRegenRate;
            @JsonProperty("resourceType")
            private String resourceType;
            @JsonProperty("resourceValue")
            private Double resourceValue;
            @JsonProperty("spellVamp")
            private Double spellVamp;
            @JsonProperty("tenacity")
            private Double tenacity;
        }

        @NoArgsConstructor
        @Data
        public static class FullRunesDto {
            @JsonProperty("generalRunes")
            private List<GeneralRunesDto> generalRunes;
            @JsonProperty("keystone")
            private KeystoneDto keystone;
            @JsonProperty("primaryRuneTree")
            private PrimaryRuneTreeDto primaryRuneTree;
            @JsonProperty("secondaryRuneTree")
            private SecondaryRuneTreeDto secondaryRuneTree;
            @JsonProperty("statRunes")
            private List<StatRunesDto> statRunes;

            @NoArgsConstructor
            @Data
            public static class KeystoneDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private Integer id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class PrimaryRuneTreeDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private Integer id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class SecondaryRuneTreeDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private Integer id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class GeneralRunesDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private Integer id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class StatRunesDto {
                @JsonProperty("id")
                private Integer id;
                @JsonProperty("rawDescription")
                private String rawDescription;
            }
        }
    }


    @NoArgsConstructor
    @Data
    public static class AllPlayersDto {
        @JsonProperty("championName")
        private String championName;
        @JsonProperty("isBot")
        private Boolean isBot;
        @JsonProperty("isDead")
        private Boolean isDead;
        @JsonProperty("items")
        private List<?> items;
        @JsonProperty("level")
        private Integer level;
        @JsonProperty("position")
        private String position;
        @JsonProperty("rawChampionName")
        private String rawChampionName;
        @JsonProperty("rawSkinName")
        private String rawSkinName;
        @JsonProperty("respawnTimer")
        private Double respawnTimer;
        @JsonProperty("runes")
        private RunesDto runes;
        @JsonProperty("scores")
        private ScoresDto scores;
        @JsonProperty("skinID")
        private Integer skinID;
        @JsonProperty("skinName")
        private String skinName;
        @JsonProperty("summonerName")
        private String summonerName;
        @JsonProperty("summonerSpells")
        private SummonerSpellsDto summonerSpells;
        @JsonProperty("team")
        private String team;

        @NoArgsConstructor
        @Data
        public static class RunesDto {
            @JsonProperty("keystone")
            private KeystoneDto keystone;
            @JsonProperty("primaryRuneTree")
            private PrimaryRuneTreeDto primaryRuneTree;
            @JsonProperty("secondaryRuneTree")
            private SecondaryRuneTreeDto secondaryRuneTree;

            @NoArgsConstructor
            @Data
            public static class KeystoneDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private Integer id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class PrimaryRuneTreeDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private Integer id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class SecondaryRuneTreeDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("id")
                private Integer id;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }
        }

        @NoArgsConstructor
        @Data
        public static class ScoresDto {
            @JsonProperty("assists")
            private Integer assists;
            @JsonProperty("creepScore")
            private Integer creepScore;
            @JsonProperty("deaths")
            private Integer deaths;
            @JsonProperty("kills")
            private Integer kills;
            @JsonProperty("wardScore")
            private Double wardScore;
        }

        @NoArgsConstructor
        @Data
        public static class SummonerSpellsDto {
            @JsonProperty("summonerSpellOne")
            private SummonerSpellOneDto summonerSpellOne;
            @JsonProperty("summonerSpellTwo")
            private SummonerSpellTwoDto summonerSpellTwo;

            @NoArgsConstructor
            @Data
            public static class SummonerSpellOneDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }

            @NoArgsConstructor
            @Data
            public static class SummonerSpellTwoDto {
                @JsonProperty("displayName")
                private String displayName;
                @JsonProperty("rawDescription")
                private String rawDescription;
                @JsonProperty("rawDisplayName")
                private String rawDisplayName;
            }
        }
    }
}
