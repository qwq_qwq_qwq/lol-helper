package com.qwq.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 游戏记录信息
 */
@Data
public class GameRecordOutputDto {
    /**
     * 召唤师名字
     */
    private String summonerName;
    /**
     * 召唤师id
     */
    private Long summonerId;
    /**
     * 单双
     */
    private RankedInfoOutputDto rankedSolo;
    /**
     * 灵活
     */
    private RankedInfoOutputDto rankedLingHuo;
    /**
     * 平均kda，代码计算的
     */
    private Double avgKda;

    /**
     * 评价
     */
    private String assessment;
    /**
     * 具体战绩
     */
    private List<GameDetail> gameDetails;

    /**
     * 游戏详情
     */
    @Data
    public static class GameDetail {
        /**
         * 开始时间
         */
        private Date startTime;
        /**
         * 　英雄id
         */
        private Integer championId;
        /**
         * 英雄名字
         */
        private String championName;
        /**
         * 击杀数
         */
        private Integer kill;
        /**
         * 助攻数
         */
        private Integer assist;
        /**
         * 死亡数
         */
        private Integer death;
    }
}
