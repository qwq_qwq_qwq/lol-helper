package com.qwq.dto;

import lombok.Data;

import java.util.List;

@Data
public class GameRecordInputDto {
    private boolean sendToTeammate;
    private List<String> levelList;
    /**
     * 是否在游戏中
     */
    private boolean inGame;
}
