package com.qwq.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SummonerInfoOutputDto {
    private String name;
    private String puuid;
    private Integer level;
    private String privacy;
    private Integer profileIconId;
    private Integer blueEssence;
    private String area;
    /**
     * 单双
     */
    private RankedInfoOutputDto rankedSolo;
    /**
     * 灵活
     */
    private RankedInfoOutputDto rankedLingHuo;
}
