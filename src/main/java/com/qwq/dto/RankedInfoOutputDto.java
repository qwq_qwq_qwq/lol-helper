package com.qwq.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 段位信息
 */
@Data
@NoArgsConstructor
public class RankedInfoOutputDto {
    /**
     * 段位
     */
    private String rank;
    /**
     * 胜场
     */
    private Integer win;
    /**
     * 胜点
     */
    private Integer point;
}
