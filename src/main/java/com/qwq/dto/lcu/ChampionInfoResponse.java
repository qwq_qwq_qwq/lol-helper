package com.qwq.dto.lcu;

import lombok.Data;

@Data
public class ChampionInfoResponse {
    /**
     * 英雄id
     */
    private Integer id;
    /**
     * 暴走萝莉
     */
    private String name;
    /**
     * Jinx
     */
    private String alias;
    /**
     * 金克丝
     */
    private String title;
}
