package com.qwq.dto.lcu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportingEnvironmentResponse {
    /**
     * 环境 HN电信，WT网通 EDU教育 BGP双线 例如(HN1:艾欧尼亚电信一区)
     */
    private String environment;
}
