package com.qwq.dto.lcu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RiotclientV1CrashReportingEnvironmentResponse {

    @JsonProperty("environment")
    private String environment;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("userName")
    private String userName;
}
