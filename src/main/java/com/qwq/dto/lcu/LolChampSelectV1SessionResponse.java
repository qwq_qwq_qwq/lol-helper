package com.qwq.dto.lcu;

import lombok.Data;

import java.util.List;

@Data
public class LolChampSelectV1SessionResponse {
    private List<List<Actions>> actions;
    private boolean allowBattleBoost;
    private boolean allowDuplicatePicks;
    private boolean allowLockedEvents;
    private boolean allowRerolling;
    private boolean allowSkinSelection;
    private Bans bans;
    private List<String> benchChampionIds;
    private boolean benchEnabled;
    private int boostableSkinCount;
    private ChatDetails chatDetails;
    private int counter;
    private EntitledFeatureState entitledFeatureState;
    private long gameId;
    private boolean hasSimultaneousBans;
    private boolean hasSimultaneousPicks;
    private boolean isCustomGame;
    private boolean isSpectating;
    private int localPlayerCellId;
    private int lockedEventIndex;
    private List<MyTeam> myTeam;
    private int recoveryCounter;
    private int rerollsRemaining;
    private boolean skipChampionSelect;
    private List<TheirTeam> theirTeam;
    private Timer timer;
    private List<String> trades;

    @Data
    public static class Actions {
        private int actorCellId;
        private int championId;
        private boolean completed;
        private int id;
        private boolean isAllyAction;
        private boolean isInProgress;
        private String type;
    }

    @Data
    public static class Bans {

        private List<String> myTeamBans;
        private int numBans;
        private List<String> theirTeamBans;

    }

    @Data
    public static class ChatDetails {

        private String chatRoomName;
        private String chatRoomPassword;

    }

    @Data
    public static class EntitledFeatureState {

        private int additionalRerolls;
        private List<String> unlockedSkinIds;

    }

    @Data
    public static class MyTeam {
        private String assignedPosition;
        private int cellId;
        private int championId;
        private int championPickIntent;
        private String entitledFeatureType;
        private int selectedSkinId;
        private int spell1Id;
        private int spell2Id;
        private long summonerId;
        private int team;
        private int wardSkinId;
    }

    @Data
    public static class TheirTeam {
        private String assignedPosition;
        private int cellId;
        private int championId;
        private int championPickIntent;
        private String entitledFeatureType;
        private int selectedSkinId;
        private int spell1Id;
        private int spell2Id;
        private int summonerId;
        private int team;
        private int wardSkinId;
    }

    @Data
    public static class Timer {

        private long adjustedTimeLeftInPhase;
        private long internalNowInEpochMs;
        private boolean isInfinite;
        private String phase;
        private long totalTimeInPhase;

    }
}