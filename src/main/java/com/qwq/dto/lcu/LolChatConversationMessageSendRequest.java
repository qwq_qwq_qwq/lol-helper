package com.qwq.dto.lcu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class LolChatConversationMessageSendRequest {
    @JsonProperty("type")
    private String type;
    @JsonProperty("fromId")
    private String fromId;
    @JsonProperty("fromSummonerId")
    private Long fromSummonerId;
    @JsonProperty("isHistorical")
    private Boolean isHistorical;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("body")
    private String body;
}
