package com.qwq.dto.lcu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class LolUpdateRankedResponse {

    @JsonProperty("availability")
    private String availability;
    @JsonProperty("gameName")
    private String gameName;
    @JsonProperty("gameTag")
    private String gameTag;
    @JsonProperty("icon")
    private Integer icon;
    @JsonProperty("id")
    private String id;
    @JsonProperty("lastSeenOnlineTimestamp")
    private Object lastSeenOnlineTimestamp;
    @JsonProperty("lol")
    private LolDto lol;
    @JsonProperty("name")
    private String name;
    @JsonProperty("patchline")
    private String patchline;
    @JsonProperty("pid")
    private String pid;
    @JsonProperty("platformId")
    private String platformId;
    @JsonProperty("product")
    private String product;
    @JsonProperty("productName")
    private String productName;
    @JsonProperty("puuid")
    private String puuid;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("summonerId")
    private Long summonerId;
    @JsonProperty("time")
    private Integer time;

    @NoArgsConstructor
    @Data
    public static class LolDto {
        @JsonProperty("championId")
        private String championId;
        @JsonProperty("companionId")
        private String companionId;
        @JsonProperty("damageSkinId")
        private String damageSkinId;
        @JsonProperty("gameQueueType")
        private String gameQueueType;
        @JsonProperty("gameStatus")
        private String gameStatus;
        @JsonProperty("iconOverride")
        private String iconOverride;
        @JsonProperty("initSummoner")
        private String initSummoner;
        @JsonProperty("mapId")
        private String mapId;
        @JsonProperty("mapSkinId")
        private String mapSkinId;
        @JsonProperty("rankedLeagueQueue")
        private String rankedLeagueQueue;
        @JsonProperty("rankedLeagueTier")
        private String rankedLeagueTier;
        @JsonProperty("skinVariant")
        private String skinVariant;
        @JsonProperty("skinname")
        private String skinname;
    }
}
