package com.qwq.dto.lcu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class LolBlueEssenceResponse {
    @JsonProperty("lol_blue_essence")
    private Integer lolBlueEssence;
}
