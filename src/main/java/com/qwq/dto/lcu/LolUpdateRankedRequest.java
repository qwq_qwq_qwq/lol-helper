package com.qwq.dto.lcu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class LolUpdateRankedRequest {

    @JsonProperty("lol")
    private LolDto lol;

    @NoArgsConstructor
    @Data
    public static class LolDto {
        @JsonProperty("rankedLeagueQueue")
        private String rankedLeagueQueue;
        @JsonProperty("rankedLeagueTier")
        private String rankedLeagueTier;
        @JsonProperty("rankedLeagueDivision")
        private String rankedLeagueDivision;
    }
}
