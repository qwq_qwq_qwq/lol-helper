package com.qwq.dto.lcu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class LolRankedV1RankedStatsResponse {
    @JsonProperty("earnedRegaliaRewardIds")
    private List<Object> earnedRegaliaRewardIds;
    @JsonProperty("highestPreviousSeasonAchievedDivision")
    private String highestPreviousSeasonAchievedDivision;
    @JsonProperty("highestPreviousSeasonAchievedTier")
    private String highestPreviousSeasonAchievedTier;
    @JsonProperty("highestPreviousSeasonEndDivision")
    private String highestPreviousSeasonEndDivision;
    @JsonProperty("highestPreviousSeasonEndTier")
    private String highestPreviousSeasonEndTier;
    @JsonProperty("highestRankedEntry")
    private HighestRankedEntryDto highestRankedEntry;
    @JsonProperty("highestRankedEntrySR")
    private HighestRankedEntrySRDto highestRankedEntrySR;
    @JsonProperty("queueMap")
    private QueueMapDto queueMap;
    @JsonProperty("queues")
    private List<QueuesDto> queues;
    @JsonProperty("rankedRegaliaLevel")
    private Integer rankedRegaliaLevel;
    @JsonProperty("seasons")
    private SeasonsDto seasons;

    @NoArgsConstructor
    @Data
    public static class HighestRankedEntryDto {
        @JsonProperty("division")
        private String division;
        @JsonProperty("isProvisional")
        private Boolean isProvisional;
        @JsonProperty("leaguePoints")
        private Integer leaguePoints;
        @JsonProperty("losses")
        private Integer losses;
        @JsonProperty("miniSeriesProgress")
        private String miniSeriesProgress;
        @JsonProperty("previousSeasonAchievedDivision")
        private String previousSeasonAchievedDivision;
        @JsonProperty("previousSeasonAchievedTier")
        private String previousSeasonAchievedTier;
        @JsonProperty("previousSeasonEndDivision")
        private String previousSeasonEndDivision;
        @JsonProperty("previousSeasonEndTier")
        private String previousSeasonEndTier;
        @JsonProperty("provisionalGameThreshold")
        private Integer provisionalGameThreshold;
        @JsonProperty("provisionalGamesRemaining")
        private Integer provisionalGamesRemaining;
        @JsonProperty("queueType")
        private String queueType;
        @JsonProperty("ratedRating")
        private Integer ratedRating;
        @JsonProperty("ratedTier")
        private String ratedTier;
        @JsonProperty("tier")
        private String tier;
        @JsonProperty("warnings")
        private WarningsDto warnings;
        @JsonProperty("wins")
        private Integer wins;

        @NoArgsConstructor
        @Data
        public static class WarningsDto {
            @JsonProperty("daysUntilDecay")
            private Integer daysUntilDecay;
            @JsonProperty("demotionWarning")
            private Integer demotionWarning;
            @JsonProperty("displayDecayWarning")
            private Boolean displayDecayWarning;
            @JsonProperty("timeUntilInactivityStatusChanges")
            private Integer timeUntilInactivityStatusChanges;
        }
    }

    @NoArgsConstructor
    @Data
    public static class HighestRankedEntrySRDto {
        @JsonProperty("division")
        private String division;
        @JsonProperty("isProvisional")
        private Boolean isProvisional;
        @JsonProperty("leaguePoints")
        private Integer leaguePoints;
        @JsonProperty("losses")
        private Integer losses;
        @JsonProperty("miniSeriesProgress")
        private String miniSeriesProgress;
        @JsonProperty("previousSeasonAchievedDivision")
        private String previousSeasonAchievedDivision;
        @JsonProperty("previousSeasonAchievedTier")
        private String previousSeasonAchievedTier;
        @JsonProperty("previousSeasonEndDivision")
        private String previousSeasonEndDivision;
        @JsonProperty("previousSeasonEndTier")
        private String previousSeasonEndTier;
        @JsonProperty("provisionalGameThreshold")
        private Integer provisionalGameThreshold;
        @JsonProperty("provisionalGamesRemaining")
        private Integer provisionalGamesRemaining;
        @JsonProperty("queueType")
        private String queueType;
        @JsonProperty("ratedRating")
        private Integer ratedRating;
        @JsonProperty("ratedTier")
        private String ratedTier;
        @JsonProperty("tier")
        private String tier;
        @JsonProperty("warnings")
        private WarningsDto warnings;
        @JsonProperty("wins")
        private Integer wins;

        @NoArgsConstructor
        @Data
        public static class WarningsDto {
            @JsonProperty("daysUntilDecay")
            private Integer daysUntilDecay;
            @JsonProperty("demotionWarning")
            private Integer demotionWarning;
            @JsonProperty("displayDecayWarning")
            private Boolean displayDecayWarning;
            @JsonProperty("timeUntilInactivityStatusChanges")
            private Integer timeUntilInactivityStatusChanges;
        }
    }

    @NoArgsConstructor
    @Data
    public static class QueueMapDto {
        @JsonProperty("RANKED_FLEX_SR")
        private RANKEDFLEXSRDto rankedFlexSr;
        @JsonProperty("RANKED_SOLO_5x5")
        private RANKEDSOLO5x5Dto rankedSolo5x5;
        @JsonProperty("RANKED_TFT")
        private RANKEDTFTDto rankedTft;
        @JsonProperty("RANKED_TFT_PAIRS")
        private RANKEDTFTPAIRSDto rankedTftPairs;
        @JsonProperty("RANKED_TFT_TURBO")
        private RANKEDTFTTURBODto rankedTftTurbo;

        @NoArgsConstructor
        @Data
        public static class RANKEDFLEXSRDto {
            @JsonProperty("division")
            private String division;
            @JsonProperty("isProvisional")
            private Boolean isProvisional;
            @JsonProperty("leaguePoints")
            private Integer leaguePoints;
            @JsonProperty("losses")
            private Integer losses;
            @JsonProperty("miniSeriesProgress")
            private String miniSeriesProgress;
            @JsonProperty("previousSeasonAchievedDivision")
            private String previousSeasonAchievedDivision;
            @JsonProperty("previousSeasonAchievedTier")
            private String previousSeasonAchievedTier;
            @JsonProperty("previousSeasonEndDivision")
            private String previousSeasonEndDivision;
            @JsonProperty("previousSeasonEndTier")
            private String previousSeasonEndTier;
            @JsonProperty("provisionalGameThreshold")
            private Integer provisionalGameThreshold;
            @JsonProperty("provisionalGamesRemaining")
            private Integer provisionalGamesRemaining;
            @JsonProperty("queueType")
            private String queueType;
            @JsonProperty("ratedRating")
            private Integer ratedRating;
            @JsonProperty("ratedTier")
            private String ratedTier;
            @JsonProperty("tier")
            private String tier;
            @JsonProperty("warnings")
            private Object warnings;
            @JsonProperty("wins")
            private Integer wins;
        }

        @NoArgsConstructor
        @Data
        public static class RANKEDSOLO5x5Dto {
            @JsonProperty("division")
            private String division;
            @JsonProperty("isProvisional")
            private Boolean isProvisional;
            @JsonProperty("leaguePoints")
            private Integer leaguePoints;
            @JsonProperty("losses")
            private Integer losses;
            @JsonProperty("miniSeriesProgress")
            private String miniSeriesProgress;
            @JsonProperty("previousSeasonAchievedDivision")
            private String previousSeasonAchievedDivision;
            @JsonProperty("previousSeasonAchievedTier")
            private String previousSeasonAchievedTier;
            @JsonProperty("previousSeasonEndDivision")
            private String previousSeasonEndDivision;
            @JsonProperty("previousSeasonEndTier")
            private String previousSeasonEndTier;
            @JsonProperty("provisionalGameThreshold")
            private Integer provisionalGameThreshold;
            @JsonProperty("provisionalGamesRemaining")
            private Integer provisionalGamesRemaining;
            @JsonProperty("queueType")
            private String queueType;
            @JsonProperty("ratedRating")
            private Integer ratedRating;
            @JsonProperty("ratedTier")
            private String ratedTier;
            @JsonProperty("tier")
            private String tier;
            @JsonProperty("warnings")
            private WarningsDto warnings;
            @JsonProperty("wins")
            private Integer wins;

            @NoArgsConstructor
            @Data
            public static class WarningsDto {
                @JsonProperty("daysUntilDecay")
                private Integer daysUntilDecay;
                @JsonProperty("demotionWarning")
                private Integer demotionWarning;
                @JsonProperty("displayDecayWarning")
                private Boolean displayDecayWarning;
                @JsonProperty("timeUntilInactivityStatusChanges")
                private Integer timeUntilInactivityStatusChanges;
            }
        }

        @NoArgsConstructor
        @Data
        public static class RANKEDTFTDto {
            @JsonProperty("division")
            private String division;
            @JsonProperty("isProvisional")
            private Boolean isProvisional;
            @JsonProperty("leaguePoints")
            private Integer leaguePoints;
            @JsonProperty("losses")
            private Integer losses;
            @JsonProperty("miniSeriesProgress")
            private String miniSeriesProgress;
            @JsonProperty("previousSeasonAchievedDivision")
            private String previousSeasonAchievedDivision;
            @JsonProperty("previousSeasonAchievedTier")
            private String previousSeasonAchievedTier;
            @JsonProperty("previousSeasonEndDivision")
            private String previousSeasonEndDivision;
            @JsonProperty("previousSeasonEndTier")
            private String previousSeasonEndTier;
            @JsonProperty("provisionalGameThreshold")
            private Integer provisionalGameThreshold;
            @JsonProperty("provisionalGamesRemaining")
            private Integer provisionalGamesRemaining;
            @JsonProperty("queueType")
            private String queueType;
            @JsonProperty("ratedRating")
            private Integer ratedRating;
            @JsonProperty("ratedTier")
            private String ratedTier;
            @JsonProperty("tier")
            private String tier;
            @JsonProperty("warnings")
            private Object warnings;
            @JsonProperty("wins")
            private Integer wins;
        }

        @NoArgsConstructor
        @Data
        public static class RANKEDTFTPAIRSDto {
            @JsonProperty("division")
            private String division;
            @JsonProperty("isProvisional")
            private Boolean isProvisional;
            @JsonProperty("leaguePoints")
            private Integer leaguePoints;
            @JsonProperty("losses")
            private Integer losses;
            @JsonProperty("miniSeriesProgress")
            private String miniSeriesProgress;
            @JsonProperty("previousSeasonAchievedDivision")
            private String previousSeasonAchievedDivision;
            @JsonProperty("previousSeasonAchievedTier")
            private String previousSeasonAchievedTier;
            @JsonProperty("previousSeasonEndDivision")
            private String previousSeasonEndDivision;
            @JsonProperty("previousSeasonEndTier")
            private String previousSeasonEndTier;
            @JsonProperty("provisionalGameThreshold")
            private Integer provisionalGameThreshold;
            @JsonProperty("provisionalGamesRemaining")
            private Integer provisionalGamesRemaining;
            @JsonProperty("queueType")
            private String queueType;
            @JsonProperty("ratedRating")
            private Integer ratedRating;
            @JsonProperty("ratedTier")
            private String ratedTier;
            @JsonProperty("tier")
            private String tier;
            @JsonProperty("warnings")
            private Object warnings;
            @JsonProperty("wins")
            private Integer wins;
        }

        @NoArgsConstructor
        @Data
        public static class RANKEDTFTTURBODto {
            @JsonProperty("division")
            private String division;
            @JsonProperty("isProvisional")
            private Boolean isProvisional;
            @JsonProperty("leaguePoints")
            private Integer leaguePoints;
            @JsonProperty("losses")
            private Integer losses;
            @JsonProperty("miniSeriesProgress")
            private String miniSeriesProgress;
            @JsonProperty("previousSeasonAchievedDivision")
            private String previousSeasonAchievedDivision;
            @JsonProperty("previousSeasonAchievedTier")
            private String previousSeasonAchievedTier;
            @JsonProperty("previousSeasonEndDivision")
            private String previousSeasonEndDivision;
            @JsonProperty("previousSeasonEndTier")
            private String previousSeasonEndTier;
            @JsonProperty("provisionalGameThreshold")
            private Integer provisionalGameThreshold;
            @JsonProperty("provisionalGamesRemaining")
            private Integer provisionalGamesRemaining;
            @JsonProperty("queueType")
            private String queueType;
            @JsonProperty("ratedRating")
            private Integer ratedRating;
            @JsonProperty("ratedTier")
            private String ratedTier;
            @JsonProperty("tier")
            private String tier;
            @JsonProperty("warnings")
            private Object warnings;
            @JsonProperty("wins")
            private Integer wins;
        }
    }

    @NoArgsConstructor
    @Data
    public static class SeasonsDto {
        @JsonProperty("RANKED_FLEX_SR")
        private RANKEDFLEXSRDto rankedFlexSr;
        @JsonProperty("RANKED_SOLO_5x5")
        private RANKEDSOLO5x5Dto rankedSolo5x5;
        @JsonProperty("RANKED_TFT")
        private RANKEDTFTDto rankedTft;
        @JsonProperty("RANKED_TFT_PAIRS")
        private RANKEDTFTPAIRSDto rankedTftPairs;
        @JsonProperty("RANKED_TFT_TURBO")
        private RANKEDTFTTURBODto rankedTftTurbo;

        @NoArgsConstructor
        @Data
        public static class RANKEDFLEXSRDto {
            @JsonProperty("currentSeasonEnd")
            private Long currentSeasonEnd;
            @JsonProperty("currentSeasonId")
            private Integer currentSeasonId;
            @JsonProperty("nextSeasonStart")
            private Integer nextSeasonStart;
        }

        @NoArgsConstructor
        @Data
        public static class RANKEDSOLO5x5Dto {
            @JsonProperty("currentSeasonEnd")
            private Long currentSeasonEnd;
            @JsonProperty("currentSeasonId")
            private Integer currentSeasonId;
            @JsonProperty("nextSeasonStart")
            private Integer nextSeasonStart;
        }

        @NoArgsConstructor
        @Data
        public static class RANKEDTFTDto {
            @JsonProperty("currentSeasonEnd")
            private Long currentSeasonEnd;
            @JsonProperty("currentSeasonId")
            private Integer currentSeasonId;
            @JsonProperty("nextSeasonStart")
            private Integer nextSeasonStart;
        }

        @NoArgsConstructor
        @Data
        public static class RANKEDTFTPAIRSDto {
            @JsonProperty("currentSeasonEnd")
            private Long currentSeasonEnd;
            @JsonProperty("currentSeasonId")
            private Integer currentSeasonId;
            @JsonProperty("nextSeasonStart")
            private Integer nextSeasonStart;
        }

        @NoArgsConstructor
        @Data
        public static class RANKEDTFTTURBODto {
            @JsonProperty("currentSeasonEnd")
            private Long currentSeasonEnd;
            @JsonProperty("currentSeasonId")
            private Integer currentSeasonId;
            @JsonProperty("nextSeasonStart")
            private Integer nextSeasonStart;
        }
    }

    @NoArgsConstructor
    @Data
    public static class QueuesDto {
        @JsonProperty("division")
        private String division;
        @JsonProperty("isProvisional")
        private Boolean isProvisional;
        @JsonProperty("leaguePoints")
        private Integer leaguePoints;
        @JsonProperty("losses")
        private Integer losses;
        @JsonProperty("miniSeriesProgress")
        private String miniSeriesProgress;
        @JsonProperty("previousSeasonAchievedDivision")
        private String previousSeasonAchievedDivision;
        @JsonProperty("previousSeasonAchievedTier")
        private String previousSeasonAchievedTier;
        @JsonProperty("previousSeasonEndDivision")
        private String previousSeasonEndDivision;
        @JsonProperty("previousSeasonEndTier")
        private String previousSeasonEndTier;
        @JsonProperty("provisionalGameThreshold")
        private Integer provisionalGameThreshold;
        @JsonProperty("provisionalGamesRemaining")
        private Integer provisionalGamesRemaining;
        @JsonProperty("queueType")
        private String queueType;
        @JsonProperty("ratedRating")
        private Integer ratedRating;
        @JsonProperty("ratedTier")
        private String ratedTier;
        @JsonProperty("tier")
        private String tier;
        @JsonProperty("warnings")
        private WarningsDto warnings;
        @JsonProperty("wins")
        private Integer wins;

        @NoArgsConstructor
        @Data
        public static class WarningsDto {
            @JsonProperty("daysUntilDecay")
            private Integer daysUntilDecay;
            @JsonProperty("demotionWarning")
            private Integer demotionWarning;
            @JsonProperty("displayDecayWarning")
            private Boolean displayDecayWarning;
            @JsonProperty("timeUntilInactivityStatusChanges")
            private Integer timeUntilInactivityStatusChanges;
        }
    }
}
