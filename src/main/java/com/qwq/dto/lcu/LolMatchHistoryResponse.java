package com.qwq.dto.lcu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@NoArgsConstructor
@Data
public class LolMatchHistoryResponse {

    @JsonProperty("accountId")
    private Long accountId;
    @JsonProperty("games")
    private GamesDto games;
    @JsonProperty("platformId")
    private String platformId;

    @NoArgsConstructor
    @Data
    public static class GamesDto {
        @JsonProperty("gameBeginDate")
        private String gameBeginDate;
        @JsonProperty("gameCount")
        private Integer gameCount;
        @JsonProperty("gameEndDate")
        private String gameEndDate;
        @JsonProperty("gameIndexBegin")
        private Integer gameIndexBegin;
        @JsonProperty("gameIndexEnd")
        private Integer gameIndexEnd;
        @JsonProperty("games")
        private List<GameDetailsDto> gamesDetails;

        @NoArgsConstructor
        @Data
        public static class GameDetailsDto {
            @JsonProperty("gameCreation")
            private Long gameCreation;
            @JsonProperty("gameCreationDate")
            private String gameCreationDate;
            @JsonProperty("gameDuration")
            private Integer gameDuration;
            @JsonProperty("gameId")
            private Long gameId;
            @JsonProperty("gameMode")
            private String gameMode;
            @JsonProperty("gameType")
            private String gameType;
            @JsonProperty("gameVersion")
            private String gameVersion;
            @JsonProperty("mapId")
            private Integer mapId;
            @JsonProperty("participantIdentities")
            private List<ParticipantIdentitiesDto> participantIdentities;
            @JsonProperty("participants")
            private List<ParticipantsDto> participants;
            @JsonProperty("platformId")
            private String platformId;
            @JsonProperty("queueId")
            private Integer queueId;
            @JsonProperty("seasonId")
            private Integer seasonId;
            @JsonProperty("teams")
            private List<?> teams;

            @NoArgsConstructor
            @Data
            public static class ParticipantIdentitiesDto {
                @JsonProperty("participantId")
                private Integer participantId;
                @JsonProperty("player")
                private ParticipantIdentitiesDto.PlayerDto player;

                @NoArgsConstructor
                @Data
                public static class PlayerDto {
                    @JsonProperty("accountId")
                    private Long accountId;
                    @JsonProperty("currentAccountId")
                    private Long currentAccountId;
                    @JsonProperty("currentPlatformId")
                    private String currentPlatformId;
                    @JsonProperty("matchHistoryUri")
                    private String matchHistoryUri;
                    @JsonProperty("platformId")
                    private String platformId;
                    @JsonProperty("profileIcon")
                    private Integer profileIcon;
                    @JsonProperty("summonerId")
                    private Long summonerId;
                    @JsonProperty("summonerName")
                    private String summonerName;
                }
            }

            @NoArgsConstructor
            @Data
            public static class ParticipantsDto {
                @JsonProperty("championId")
                private Integer championId;
                @JsonProperty("highestAchievedSeasonTier")
                private String highestAchievedSeasonTier;
                @JsonProperty("participantId")
                private Integer participantId;
                @JsonProperty("spell1Id")
                private Integer spell1Id;
                @JsonProperty("spell2Id")
                private Integer spell2Id;
                @JsonProperty("stats")
                private StatsDto stats;
                @JsonProperty("teamId")
                private Integer teamId;
                @JsonProperty("timeline")
                private TimelineDto timeline;

                @NoArgsConstructor
                @Data
                public static class StatsDto {
                    @JsonProperty("assists")
                    private Integer assists;
                    @JsonProperty("causedEarlySurrender")
                    private Boolean causedEarlySurrender;
                    @JsonProperty("champLevel")
                    private Integer champLevel;
                    @JsonProperty("combatPlayerScore")
                    private Integer combatPlayerScore;
                    @JsonProperty("damageDealtToObjectives")
                    private Integer damageDealtToObjectives;
                    @JsonProperty("damageDealtToTurrets")
                    private Integer damageDealtToTurrets;
                    @JsonProperty("damageSelfMitigated")
                    private Integer damageSelfMitigated;
                    @JsonProperty("deaths")
                    private Integer deaths;
                    @JsonProperty("doubleKills")
                    private Integer doubleKills;
                    @JsonProperty("earlySurrenderAccomplice")
                    private Boolean earlySurrenderAccomplice;
                    @JsonProperty("firstBloodAssist")
                    private Boolean firstBloodAssist;
                    @JsonProperty("firstBloodKill")
                    private Boolean firstBloodKill;
                    @JsonProperty("firstInhibitorAssist")
                    private Boolean firstInhibitorAssist;
                    @JsonProperty("firstInhibitorKill")
                    private Boolean firstInhibitorKill;
                    @JsonProperty("firstTowerAssist")
                    private Boolean firstTowerAssist;
                    @JsonProperty("firstTowerKill")
                    private Boolean firstTowerKill;
                    @JsonProperty("gameEndedInEarlySurrender")
                    private Boolean gameEndedInEarlySurrender;
                    @JsonProperty("gameEndedInSurrender")
                    private Boolean gameEndedInSurrender;
                    @JsonProperty("goldEarned")
                    private Integer goldEarned;
                    @JsonProperty("goldSpent")
                    private Integer goldSpent;
                    @JsonProperty("inhibitorKills")
                    private Integer inhibitorKills;
                    @JsonProperty("item0")
                    private Integer item0;
                    @JsonProperty("item1")
                    private Integer item1;
                    @JsonProperty("item2")
                    private Integer item2;
                    @JsonProperty("item3")
                    private Integer item3;
                    @JsonProperty("item4")
                    private Integer item4;
                    @JsonProperty("item5")
                    private Integer item5;
                    @JsonProperty("item6")
                    private Integer item6;
                    @JsonProperty("killingSprees")
                    private Integer killingSprees;
                    @JsonProperty("kills")
                    private Integer kills;
                    @JsonProperty("largestCriticalStrike")
                    private Integer largestCriticalStrike;
                    @JsonProperty("largestKillingSpree")
                    private Integer largestKillingSpree;
                    @JsonProperty("largestMultiKill")
                    private Integer largestMultiKill;
                    @JsonProperty("longestTimeSpentLiving")
                    private Integer longestTimeSpentLiving;
                    @JsonProperty("magicDamageDealt")
                    private Integer magicDamageDealt;
                    @JsonProperty("magicDamageDealtToChampions")
                    private Integer magicDamageDealtToChampions;
                    @JsonProperty("magicalDamageTaken")
                    private Integer magicalDamageTaken;
                    @JsonProperty("neutralMinionsKilled")
                    private Integer neutralMinionsKilled;
                    @JsonProperty("neutralMinionsKilledEnemyJungle")
                    private Integer neutralMinionsKilledEnemyJungle;
                    @JsonProperty("neutralMinionsKilledTeamJungle")
                    private Integer neutralMinionsKilledTeamJungle;
                    @JsonProperty("objectivePlayerScore")
                    private Integer objectivePlayerScore;
                    @JsonProperty("participantId")
                    private Integer participantId;
                    @JsonProperty("pentaKills")
                    private Integer pentaKills;
                    @JsonProperty("perk0")
                    private Integer perk0;
                    @JsonProperty("perk0Var1")
                    private Integer perk0Var1;
                    @JsonProperty("perk0Var2")
                    private Integer perk0Var2;
                    @JsonProperty("perk0Var3")
                    private Integer perk0Var3;
                    @JsonProperty("perk1")
                    private Integer perk1;
                    @JsonProperty("perk1Var1")
                    private Integer perk1Var1;
                    @JsonProperty("perk1Var2")
                    private Integer perk1Var2;
                    @JsonProperty("perk1Var3")
                    private Integer perk1Var3;
                    @JsonProperty("perk2")
                    private Integer perk2;
                    @JsonProperty("perk2Var1")
                    private Integer perk2Var1;
                    @JsonProperty("perk2Var2")
                    private Integer perk2Var2;
                    @JsonProperty("perk2Var3")
                    private Integer perk2Var3;
                    @JsonProperty("perk3")
                    private Integer perk3;
                    @JsonProperty("perk3Var1")
                    private Integer perk3Var1;
                    @JsonProperty("perk3Var2")
                    private Integer perk3Var2;
                    @JsonProperty("perk3Var3")
                    private Integer perk3Var3;
                    @JsonProperty("perk4")
                    private Integer perk4;
                    @JsonProperty("perk4Var1")
                    private Integer perk4Var1;
                    @JsonProperty("perk4Var2")
                    private Integer perk4Var2;
                    @JsonProperty("perk4Var3")
                    private Integer perk4Var3;
                    @JsonProperty("perk5")
                    private Integer perk5;
                    @JsonProperty("perk5Var1")
                    private Integer perk5Var1;
                    @JsonProperty("perk5Var2")
                    private Integer perk5Var2;
                    @JsonProperty("perk5Var3")
                    private Integer perk5Var3;
                    @JsonProperty("perkPrimaryStyle")
                    private Integer perkPrimaryStyle;
                    @JsonProperty("perkSubStyle")
                    private Integer perkSubStyle;
                    @JsonProperty("physicalDamageDealt")
                    private Integer physicalDamageDealt;
                    @JsonProperty("physicalDamageDealtToChampions")
                    private Integer physicalDamageDealtToChampions;
                    @JsonProperty("physicalDamageTaken")
                    private Integer physicalDamageTaken;
                    @JsonProperty("playerScore0")
                    private Integer playerScore0;
                    @JsonProperty("playerScore1")
                    private Integer playerScore1;
                    @JsonProperty("playerScore2")
                    private Integer playerScore2;
                    @JsonProperty("playerScore3")
                    private Integer playerScore3;
                    @JsonProperty("playerScore4")
                    private Integer playerScore4;
                    @JsonProperty("playerScore5")
                    private Integer playerScore5;
                    @JsonProperty("playerScore6")
                    private Integer playerScore6;
                    @JsonProperty("playerScore7")
                    private Integer playerScore7;
                    @JsonProperty("playerScore8")
                    private Integer playerScore8;
                    @JsonProperty("playerScore9")
                    private Integer playerScore9;
                    @JsonProperty("quadraKills")
                    private Integer quadraKills;
                    @JsonProperty("sightWardsBoughtInGame")
                    private Integer sightWardsBoughtInGame;
                    @JsonProperty("teamEarlySurrendered")
                    private Boolean teamEarlySurrendered;
                    @JsonProperty("timeCCingOthers")
                    private Integer timeCCingOthers;
                    @JsonProperty("totalDamageDealt")
                    private Integer totalDamageDealt;
                    @JsonProperty("totalDamageDealtToChampions")
                    private Integer totalDamageDealtToChampions;
                    @JsonProperty("totalDamageTaken")
                    private Integer totalDamageTaken;
                    @JsonProperty("totalHeal")
                    private Integer totalHeal;
                    @JsonProperty("totalMinionsKilled")
                    private Integer totalMinionsKilled;
                    @JsonProperty("totalPlayerScore")
                    private Integer totalPlayerScore;
                    @JsonProperty("totalScoreRank")
                    private Integer totalScoreRank;
                    @JsonProperty("totalTimeCrowdControlDealt")
                    private Integer totalTimeCrowdControlDealt;
                    @JsonProperty("totalUnitsHealed")
                    private Integer totalUnitsHealed;
                    @JsonProperty("tripleKills")
                    private Integer tripleKills;
                    @JsonProperty("trueDamageDealt")
                    private Integer trueDamageDealt;
                    @JsonProperty("trueDamageDealtToChampions")
                    private Integer trueDamageDealtToChampions;
                    @JsonProperty("trueDamageTaken")
                    private Integer trueDamageTaken;
                    @JsonProperty("turretKills")
                    private Integer turretKills;
                    @JsonProperty("unrealKills")
                    private Integer unrealKills;
                    @JsonProperty("visionScore")
                    private Integer visionScore;
                    @JsonProperty("visionWardsBoughtInGame")
                    private Integer visionWardsBoughtInGame;
                    @JsonProperty("wardsKilled")
                    private Integer wardsKilled;
                    @JsonProperty("wardsPlaced")
                    private Integer wardsPlaced;
                    @JsonProperty("win")
                    private Boolean win;
                }

                @NoArgsConstructor
                @Data
                public static class TimelineDto {
                    @JsonProperty("creepsPerMinDeltas")
                    private CreepsPerMinDeltasDto creepsPerMinDeltas;
                    @JsonProperty("csDiffPerMinDeltas")
                    private CsDiffPerMinDeltasDto csDiffPerMinDeltas;
                    @JsonProperty("damageTakenDiffPerMinDeltas")
                    private DamageTakenDiffPerMinDeltasDto damageTakenDiffPerMinDeltas;
                    @JsonProperty("damageTakenPerMinDeltas")
                    private DamageTakenPerMinDeltasDto damageTakenPerMinDeltas;
                    @JsonProperty("goldPerMinDeltas")
                    private GoldPerMinDeltasDto goldPerMinDeltas;
                    @JsonProperty("lane")
                    private String lane;
                    @JsonProperty("participantId")
                    private Integer participantId;
                    @JsonProperty("role")
                    private String role;
                    @JsonProperty("xpDiffPerMinDeltas")
                    private XpDiffPerMinDeltasDto xpDiffPerMinDeltas;
                    @JsonProperty("xpPerMinDeltas")
                    private XpPerMinDeltasDto xpPerMinDeltas;

                    @NoArgsConstructor
                    @Data
                    public static class CreepsPerMinDeltasDto {
                    }

                    @NoArgsConstructor
                    @Data
                    public static class CsDiffPerMinDeltasDto {
                    }

                    @NoArgsConstructor
                    @Data
                    public static class DamageTakenDiffPerMinDeltasDto {
                    }

                    @NoArgsConstructor
                    @Data
                    public static class DamageTakenPerMinDeltasDto {
                        @JsonProperty("0-10")
                        private Double $010;
                        @JsonProperty("10-20")
                        private Double $1020;
                    }

                    @NoArgsConstructor
                    @Data
                    public static class GoldPerMinDeltasDto {
                    }

                    @NoArgsConstructor
                    @Data
                    public static class XpDiffPerMinDeltasDto {
                    }

                    @NoArgsConstructor
                    @Data
                    public static class XpPerMinDeltasDto {
                        @JsonProperty("0-10")
                        private Double $010;
                        @JsonProperty("10-20")
                        private Double $1020;
                    }
                }
            }
        }
    }
}
