package com.qwq.dto.lcu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class LolChatConversationMessageSendResponse {

    @JsonProperty("body")
    private String body;
    @JsonProperty("fromId")
    private String fromId;
    @JsonProperty("fromPid")
    private String fromPid;
    @JsonProperty("fromSummonerId")
    private Long fromSummonerId;
    @JsonProperty("id")
    private String id;
    @JsonProperty("isHistorical")
    private Boolean isHistorical;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("type")
    private String type;
}
