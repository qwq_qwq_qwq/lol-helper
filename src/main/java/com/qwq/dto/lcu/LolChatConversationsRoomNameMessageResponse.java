package com.qwq.dto.lcu;

import lombok.Data;

@Data
public class LolChatConversationsRoomNameMessageResponse {
    private String body;

    private String fromId;

    private String fromPid;

    private Long fromSummonerId;

    private String id;

    private boolean isHistorical;

    private String timestamp;

    private String type;

}
