package com.qwq.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendRoomMessageInputDto {
    private String roomName;
    private String text;
    private String summonerId;
}
