package com.qwq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class lolApplication {
    public static void main(String[] args) {
        SpringApplication.run(lolApplication.class, args);
    }
}
