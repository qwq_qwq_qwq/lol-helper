package com.qwq.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringBootTest
public class LcuConfigTest {

    @Test
    void selectPortAndToken() {
        String execResult = " c:/software/lol/lol(26)/LeagueClient/LeagueClientUx.exe \"--riotclient-auth-token=lJm9AFLzcD9z_OsArw3hRg\" \"--riotclient-app-port=57283\" \"--riotclient-tencent\" \"--no-rads\" \"--disable-self-update\" \"--region=TENCENT\" \"--locale=zh_CN\" \"--t.lcdshost=hn1-sz-feapp.lol.qq.com\" \"--t.chathost=hn1-sz-ejabberd.lol.qq.com\" \"--t.lq=https://hn1-sz-login.lol.qq.com:8443\" \"--t.storeurl=https://hn1-sr.lol.qq.com:8443\" \"--t.rmsurl=wss://sz-rms-bcs.lol.qq.com:443\" \"--rso-auth.url=https://prod-rso.lol.qq.com:3000\" \"--rso_platform_id=HN1\" \"--rso-auth.client=lol\" \"--t.location=loltencent.sz.HN1\" \"--tglog-endpoint=https://tglogsz.datamore.qq.com/lolcli/report/\" \"--t.league_edge_url=https://ledge-hn1.lol.qq.com:22019\" \"--ccs=https://cc-hn1.lol.qq.com:8093\" \"--dradis-endpoint=http://some.url\" \"--remoting-auth-token=LmGt5DHABULVk1ZcJPA-eQ\" \"--app-port=57321\" \"--install-directory=c:\\software\\lol\\lol(26)\\LeagueClient\" \"--app-name=LeagueClient\" \"--ux-name=LeagueClientUx\" \"--ux-helper-name=LeagueClientUxHelper\" \"--log-dir=LeagueClient Logs\" \"--crash-reporting=none\" \"--crash-environment=HN1\" \"--app-log-file-path=c:/software/lol/lol(26)/LeagueClient/../Game/Logs/LeagueClient Logs/2022-03-31T12-53-05_11304_LeagueClient.log\" \"--app-pid=11304\" \"--output-base-dir=c:/software/lol/lol(26)/LeagueClient/../Game\" \"--no-proxy-server\"";
        Pattern pattern1 = Pattern.compile("\"--remoting-auth-token=.+?\"");
        Matcher matcher1 = pattern1.matcher(execResult);
        String temp = null;
        if (matcher1.find()) {
            temp = matcher1.group(0);
        }
        Assertions.assertNotNull(temp);
        String token = temp.split("=")[1].replace("\"", "");
        Assertions.assertEquals("LmGt5DHABULVk1ZcJPA-eQ", token);
        temp = null;
        Pattern pattern2 = Pattern.compile("\"--app-port=.+?\"");
        Matcher matcher2 = pattern2.matcher(execResult);
        if (matcher2.find()) {
            temp = matcher2.group(0);
        }
        Assertions.assertNotNull(temp);
        String port = temp.split("=")[1].replace("\"", "");
        Assertions.assertEquals("57321", port);
        System.out.println(token + ":" + port);
    }
}
