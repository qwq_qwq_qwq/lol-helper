package com.qwq.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class JsonTest {
    @Autowired
    private ObjectMapper objectMapper;

    @SneakyThrows
    @Test
    void writeTest() {
        Book book = new Book(1, "c#开发入门", false);
        System.out.println(objectMapper.writeValueAsString(book));
    }

    @SneakyThrows
    @Test
    void testReadValue() {
        String jsonValue = "{\"id\":1,\"name\":\"c#开发入门\",\"deleted\":true}";
        System.out.println(objectMapper.readValue(jsonValue, Book.class));
    }

    @SneakyThrows
    @Test
    void jsonPropertyTest() {
        People people = new People(2, "alice", false);
        String s = objectMapper.writeValueAsString(people);
        System.out.println(s);
        s = "{\"id\":2,\"deleted\":false,\"na\":\"petter\"}";
        System.out.println(objectMapper.readValue(s, People.class));
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Book {
    private Integer id;
    private String name;
    private boolean Deleted;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class People {
    private Integer id;
    @JsonProperty("na")
    private String name;
    private boolean Deleted;
}
