package com.qwq.test;

import com.qwq.infrastructure.api.ApiResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ApiResultTest {
    /*
     * 请求服务端接口成功，业务处理成功测试
     * */
    @Test
    void success() {
        ApiResult<Object> apiResult = ApiResult.success();
        Assertions.assertNotNull(apiResult);
        Assertions.assertEquals("ok", apiResult.getMessage());
        Assertions.assertEquals(0, apiResult.getErrorCode().intValue());
        Assertions.assertTrue(apiResult.isSuccess());
    }

    /*
     * 请求服务端接口成功，业务处理成功,且服务端返回数据 测试
     * */
    @Test
    void successAndReturnData() {
        String data = "hello world";
        ApiResult<String> apiResult = ApiResult.success(data);
        Assertions.assertNotNull(apiResult);
        Assertions.assertEquals("ok", apiResult.getMessage());
        Assertions.assertEquals(0, apiResult.getErrorCode().intValue());
        Assertions.assertTrue(apiResult.isSuccess());
        Assertions.assertEquals(data, apiResult.getResult());
    }

    /*
     *   请求服务端成功 业务处理失败
     * */
    @Test
    void fail() {
        ApiResult<Object> apiResult = ApiResult.fail(200, "参数不能为空");
        Assertions.assertNotNull(apiResult);
        Assertions.assertEquals("参数不能为空", apiResult.getMessage());
        Assertions.assertEquals(200, apiResult.getErrorCode().intValue());
        Assertions.assertFalse(apiResult.isSuccess());
        Assertions.assertNull(apiResult.getResult());
    }
}
