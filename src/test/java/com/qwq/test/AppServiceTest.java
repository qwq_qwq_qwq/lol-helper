package com.qwq.test;

import com.qwq.dto.GameRecordOutputDto;
import com.qwq.service.AppService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

@SpringBootTest
public class AppServiceTest {
    @Autowired
    private AppService _appService;

    @Test
    void getAreaTest() {
        String area = _appService.getArea("HN1");
        Assertions.assertEquals("艾欧尼亚", area);
        area = _appService.getArea("HN19");
        Assertions.assertEquals("皮城警备", area);
        area = _appService.getArea("EDU1");
        Assertions.assertEquals("教育网专区", area);
        area = _appService.getArea("none");
        Assertions.assertNull(area);
    }

    @Test
    void KdaSortedTest() {
        GameRecordOutputDto outputDto1 = new GameRecordOutputDto();
        GameRecordOutputDto outputDto2 = new GameRecordOutputDto();
        GameRecordOutputDto outputDto3 = new GameRecordOutputDto();
        GameRecordOutputDto outputDto4 = new GameRecordOutputDto();
        GameRecordOutputDto outputDto5 = new GameRecordOutputDto();
        outputDto1.setAvgKda(14.2234);
        outputDto2.setAvgKda(19.0);
        outputDto3.setAvgKda(12.3);
        outputDto4.setAvgKda(2.3);
        outputDto5.setAvgKda(27.3);
        List<GameRecordOutputDto> list = new ArrayList<>(List.of(outputDto1, outputDto2, outputDto3, outputDto4, outputDto5));
        List<String> levelList = new ArrayList<>(List.of("牛马", "下等马", "中等马", "上等马", "小代"));
        // 升序排列
        list = list.stream().sorted(Comparator.comparing(GameRecordOutputDto::getAvgKda)).collect(Collectors.toList());
        for (int i = 0; i < list.size(); i++) {
            String assessment = levelList.get(i);
            list.get(i).setAssessment(assessment);
        }

        System.out.println(list);
    }

    @Test
    void sendRoomMessageTimeStampTest() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        System.out.println(simpleDateFormat.format(System.currentTimeMillis()));
    }
}
