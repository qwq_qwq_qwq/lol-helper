package com.qwq.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public class RestTemplateTest {
    @Autowired
    private RestTemplate restTemplate;

    @Test
    void get() {
        String url = "https://tieba.baidu.com/dc/common/tbs";
        String forObject = restTemplate.getForObject(url, String.class);
        System.out.println(forObject);
        BaiDuTbsResponse response = restTemplate.getForObject(url, BaiDuTbsResponse.class);
        Assertions.assertNotNull(response);
        Assertions.assertTrue(StringUtils.hasText(response.getTbs()));
        Assertions.assertEquals(0, response.getIsLogin().intValue());
    }

    @ToString
    @Setter
    @Getter
    static class BaiDuTbsResponse {
        private String tbs;
        @JsonProperty("is_login")
        private Integer IsLogin;
    }
}
